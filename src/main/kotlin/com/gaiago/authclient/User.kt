package com.gaiago.authclient

import java.lang.RuntimeException
import java.util.*

interface User {
    val email: String
    val roles: List<UserRole>
    val language: String
}

data class GaiaGoUser(
        override val email: String,
        override val roles: List<UserRole>,
        override val language: String
): User

data class MovensUser(
        val id: Int,
        override val email: String,
        override val roles: List<UserRole>,
        override val language: String,
        val community: UUID,
        val fullName: String
): User
