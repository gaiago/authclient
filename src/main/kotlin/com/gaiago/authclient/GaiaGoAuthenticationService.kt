package com.gaiago.authclient

import com.google.gson.JsonParser
import topinambur.Bearer
import topinambur.ServerResponse
import topinambur.http
import java.util.*

class GaiaGoAuthenticationService(backendHost: String): AuthenticationService {

    private val authenticationEndpoint = "$backendHost/auth/api/user"

    override fun authenticate(token: String): Profile {
        val response = authenticationEndpoint.http.get(auth = Bearer(token))

        return try {
            parseMovensProfile(response, token)
        } catch(t: Throwable) {
            parseGaiaGoProfile(response, token)
        }
    }

    private fun parseGaiaGoProfile(response: ServerResponse, token: String): GaiaGoProfile {
        val parsedBody = JsonParser.parseString(response.body).asJsonObject
        val roles = parsedBody.get("roles")?.asJsonArray?.map { UserRole.fromCode(it.asString) } ?: emptyList()
        return GaiaGoProfile(GaiaGoUser(
                parsedBody.get("email").asString,
                roles,
                parsedBody.get("language").asString
        ), token)
    }

    private fun parseMovensProfile(response: ServerResponse, token: String): MovensProfile {
        val parsedBody = JsonParser.parseString(response.body).asJsonObject
        val roles = parsedBody.get("roles").asJsonArray.map { UserRole.fromCode(it.asString) }
        val communityGuid = UUID.fromString(parsedBody.get("community").asString)
        return MovensProfile(
                MovensUser(
                        parsedBody.get("id").asInt,
                        parsedBody.get("email").asString,
                        roles,
                        parsedBody.get("language").asString,
                        communityGuid,
                        parsedBody.get("fullName").asString
                ),
                token
        )
    }
}