package com.gaiago.authclient

interface AuthenticationService {
    fun authenticate(token: String) : Profile
}
