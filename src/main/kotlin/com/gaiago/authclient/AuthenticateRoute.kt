package com.gaiago.authclient

import daikon.core.*
import org.slf4j.LoggerFactory

class AuthenticateRoute(private val authenticationService: AuthenticationService): RouteAction {
    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun handle(request: Request, response: Response, context: Context) {
        try {
            val profile = authenticationService.authenticate(credentials(request))
            request.attribute("profile", profile)
        } catch (t: Throwable) {
            logger.warn("Authentication failed")
            RequestFlow.halt(response, 401)
        }
    }

    private fun credentials(request: Request): String {
        return request.header("Authorization").split(" ").last()
    }
}