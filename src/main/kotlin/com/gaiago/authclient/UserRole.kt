package com.gaiago.authclient

enum class UserRole(val code: String) {
    END_USER("EndUser"),
    COMMUNITY_ADMIN("Admin"),
    GAIAGO_ADMIN("GaiaGoAdmin"),
    OTHER("Other");

    companion object {
        fun fromCode(code: String): UserRole {
            return values().singleOrNull { it.code == code } ?: OTHER
        }
    }
}