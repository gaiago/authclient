package com.gaiago.authclient

open class Profile(open val user: User, open val token: String) {
    fun canAccess(requiredRole: List<UserRole>): Boolean {
        return user.roles.intersect(requiredRole).isNotEmpty()
    }
}

data class MovensProfile(override val user: MovensUser, override val token: String): Profile(user, token)
data class GaiaGoProfile(override val user: GaiaGoUser, override val token: String): Profile(user, token)