package com.gaiago.authclient

import daikon.core.Context
import daikon.core.Request
import daikon.core.RequestFlow.halt
import daikon.core.Response
import daikon.core.RouteAction
import org.slf4j.LoggerFactory

class AuthorizeRoute(private val allowedRoles: List<UserRole>, private val action: RouteAction): RouteAction {
    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun handle(request: Request, response: Response, context: Context) {
        val profile = request.attribute<Profile>("profile")
        if(!profile.canAccess(allowedRoles)) {
            logger.warn("Authorization failed")
            halt(response, 403)
        }

        action.handle(request, response, context)
    }
}