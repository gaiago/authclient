package com.gaiago.authclient

import com.gaiago.authclient.UserRole.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.random.Random

class ProfileTest {

    @Test
    fun `check roles`() {
        val profile = movensProfile(listOf(COMMUNITY_ADMIN, OTHER))

        assertThat(profile.canAccess(listOf(COMMUNITY_ADMIN, END_USER))).isTrue()
        assertThat(profile.canAccess(listOf(END_USER))).isFalse()
    }

    private fun movensProfile(userRoles: List<UserRole>) =
        Profile(MovensUser(Random.nextInt(), "", userRoles, "it", UUID.randomUUID(), ""), "token")
}