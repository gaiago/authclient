package com.gaiago.authclient

import com.gaiago.authclient.UserRole.COMMUNITY_ADMIN
import com.gaiago.authclient.UserRole.END_USER
import daikon.core.*
import daikon.core.HttpStatus.FORBIDDEN_403
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.random.Random

class AuthorizeRouteTest {
    private val request: Request = mockk(relaxed = true)
    private val response: Response = mockk(relaxed = true)

    @Test
    fun `authorized user`() {
        val spyAction = SpyAction()
        every { request.attribute<Profile>("profile") } returns movensProfile(listOf(COMMUNITY_ADMIN))

        AuthorizeRoute(listOf(COMMUNITY_ADMIN), spyAction).handle(request, response, mockk())

        spyAction.verifyCalled()
    }

    @Test
    fun `unauthorized user`() {
        val spyAction = SpyAction()

        every { request.attribute<Profile>("profile") } returns movensProfile(listOf(END_USER))

        try {
            AuthorizeRoute(listOf(COMMUNITY_ADMIN), spyAction).handle(request, response, mockk())
            fail("Should throw HaltException")
        }
        catch (e: HaltException) {
            spyAction.verifyNotCalled()
            verify { response.status(FORBIDDEN_403) }
        }
    }

    private fun movensProfile(roles: List<UserRole>): Profile {
        return Profile(MovensUser(Random.nextInt(), "", roles, "it", UUID.randomUUID(), ""), "token")
    }
}

class SpyAction : RouteAction {
    var called: Boolean = false
        private set

    override fun handle(request: Request, response: Response, context: Context) {
        called = true
    }

    fun verifyNotCalled() {
        assertThat(called).isFalse().withFailMessage("Action should not be called, but was.")
    }

    fun verifyCalled() {
        assertThat(called).isTrue().withFailMessage("Action should be called, but wasn't.")
    }
}
