package com.gaiago.authclient


import daikon.HttpServer
import daikon.core.DaikonServer
import daikon.core.HttpStatus.UNAUTHORIZED_401
import daikon.core.Request
import daikon.core.RequestFlow
import daikon.core.Response
import java.util.*


class AuthenticationServer(
    val userId: Int = Random().nextInt(),
    val communityGuid: UUID = UUID.randomUUID(),
    val authToken: String = "valid_token",
    val role: UserRole = UserRole.END_USER,
    val email: String = "a@b.c",
    val name: String = "User",
    val familyName: String = "One",
    val language: String = "it",
    val isCognito: Boolean = false
) : AutoCloseable {
    private lateinit var server: DaikonServer

    companion object {
        private const val port = 9000
        const val BACKEND_HOST = "http://localhost:$port"
        const val JSON = "application/json;charset=utf-8"
    }

    fun start(): AuthenticationServer {
        server = HttpServer(port) {
            before("/auth/api/user", authenticationFilter())
            get("/auth/api/user", currentUser())
        }.start()

        return this
    }

    private fun currentUser(): (Request, Response) -> Unit {
        return { _, res ->
            res.type(JSON)
            if(isCognito) {
                res.write("""{
                    "identifier":"${UUID.randomUUID()}",
                    "email":"$email",
                    "roles":["${role.code}"],
                    "language":"$language",
                    "communityGuid":"$communityGuid",
                    "name":"$name",
                    "familyName":"$familyName"
                }""")
            }else {
                res.write("""{
                    "id":$userId,
                    "userName":"$email",
                    "email":"$email",
                    "roles":["${role.code}"],
                    "language":"$language",
                    "community":"$communityGuid",
                    "fullName":"${"$name $familyName".trim()}"
                }""")
            }
        }
    }

    private fun authenticationFilter(): (Request, Response) -> Unit {
        return { req, res ->
            if (isWrongToken(req)) {
                RequestFlow.halt(res, UNAUTHORIZED_401)
            }
        }
    }

    private fun isWrongToken(req: Request): Boolean {
        return try {
            req.header("Authorization") != "Bearer $authToken"
        } catch (e: Throwable) {
            true
        }
    }

    override fun close() {
        server.close()
    }
}