package com.gaiago.authclient

import com.gaiago.authclient.UserRole.END_USER
import daikon.core.HaltException
import daikon.core.HttpStatus.UNAUTHORIZED_401
import daikon.core.Request
import daikon.core.Response
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import java.util.*
import kotlin.random.Random

class AuthenticateRouteTest {

    private val request: Request = mockk(relaxed = true)
    private val response: Response = mockk(relaxed = true)
    private val authenticationService: AuthenticationService = mockk()

    @Test
    fun `retrieve user id`() {
        withAuthorizationHeader(request, "token")
        val profile = movensProfile()
        every { authenticationService.authenticate("token") } returns profile
        every { request.attribute<Profile>("profile") } returns profile

        AuthenticateRoute(authenticationService).handle(request, response, mockk())

        verify { request.attribute("profile", profile) }
    }

    @Test
    fun `invalid token`() {
        withAuthorizationHeader(request, "my-token")
        every { authenticationService.authenticate("my-token") } throws RuntimeException()

        try {
            AuthenticateRoute(authenticationService).handle(request, response, mockk())
            fail("Should throw HaltException")
        }
        catch (e: HaltException) {
            verify { response.status(UNAUTHORIZED_401) }
        }
    }

    private fun movensProfile(): Profile {
        return Profile(MovensUser(Random.nextInt(), "", listOf(END_USER), "it", UUID.randomUUID(), ""), "token")
    }

    private fun withAuthorizationHeader(req: Request, token: String) {
        every {req.header("Authorization") } returns "Bearer $token"
    }
}