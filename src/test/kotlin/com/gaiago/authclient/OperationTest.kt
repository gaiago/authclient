package com.gaiago.authclient

import com.google.gson.JsonParser
import daikon.HttpServer
import daikon.core.HttpStatus.FORBIDDEN_403
import daikon.core.HttpStatus.UNAUTHORIZED_401
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import topinambur.Bearer
import topinambur.http
import java.lang.ClassCastException

@Disabled
class OperationTest {
    private val host = "current.host"

    @Test
    fun `login account`() {
        asUser("a@b.c", "Password1!") { token ->
            val profile = GaiaGoAuthenticationService(host).authenticate(token)

            println(profile)
        }
    }

    @Test
    fun `authorize route`() {
        HttpServer(8889) {
            before("/", AuthenticateRoute(GaiaGoAuthenticationService(host)))
            exception(ClassCastException::class.java) { _, res, t -> res.status(FORBIDDEN_403) }
            get("/") { req, res ->
                println("community: " + req.attribute<GaiaGoProfile>("profile").user.roles)
            }
        }.start().use {
            asUser("gaiago-user", "gaiago-password") { token ->
                "http://localhost:8889/".http.get(auth = Bearer(token))
            }
        }
    }

    private fun asUser(user: String, password: String, function: (String) -> Unit) {
        userLoginToken(user, password).also {
            function(it)
            userLogoffToken(it)
        }
    }

    private fun userLoginToken(user: String, password: String): String {
        val response = "${host}/auth/api/login".http.post(
                headers = hashMapOf("Content-Type" to "application/json"),
                body = """{"username": "$user","password": "$password"}"""
        )

        println("logon = $response")

        return JsonParser
                .parseString(response.body).asJsonObject
                .get("token").asString
    }

    private fun userLogoffToken(token: String) {
        val response = "${host}/api/mobile/v1/user/logoff".http.post(
                headers = hashMapOf("Content-Type" to "application/json"),
                auth = Bearer(token))

        println("logoff = $response")
    }
}