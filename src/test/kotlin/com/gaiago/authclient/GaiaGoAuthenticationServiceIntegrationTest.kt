package com.gaiago.authclient

import com.gaiago.authclient.AuthenticationServer.Companion.BACKEND_HOST
import com.gaiago.authclient.UserRole.COMMUNITY_ADMIN
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID.randomUUID

class GaiaGoAuthenticationServiceIntegrationTest {
    private val VALID_TOKEN = "allowed_token"
    private val EXPECTED_USERID = 8

    @Test
    fun `authenticated movens user`() {
        val communityGuid = randomUUID()
        AuthenticationServer(
            userId = EXPECTED_USERID,
            authToken = VALID_TOKEN,
            role = COMMUNITY_ADMIN,
            communityGuid = communityGuid,
            email = "",
            name = "",
            familyName = ""
        ).start().use {
            val profile = GaiaGoAuthenticationService(BACKEND_HOST).authenticate(VALID_TOKEN)

            assertThat(profile).isEqualTo(
                MovensProfile(MovensUser(
                    EXPECTED_USERID,
                    "",
                    listOf(COMMUNITY_ADMIN),
                    "it",
                    communityGuid,
                    ""
                ),
                VALID_TOKEN
            ))
        }
    }

    @Test
    fun `authenticated gaiago user`() {
        val communityGuid = randomUUID()
        AuthenticationServer(
                userId = EXPECTED_USERID,
                authToken = VALID_TOKEN,
                role = UserRole.GAIAGO_ADMIN,
                communityGuid = communityGuid,
                email = "a@b.c",
                name = "nm",
                familyName = "fmlnm",
                language = "en",
                isCognito = true
        ).start().use {
            val userId = GaiaGoAuthenticationService(BACKEND_HOST).authenticate(VALID_TOKEN)

            assertThat(userId).isEqualTo(GaiaGoProfile(GaiaGoUser("a@b.c", listOf(UserRole.GAIAGO_ADMIN), "en"), VALID_TOKEN))
        }
    }

    @Test
    fun `invalid authentication`() {
        AuthenticationServer(userId = EXPECTED_USERID, authToken = VALID_TOKEN).start().use {
            assertThrows<Throwable> {
                GaiaGoAuthenticationService(BACKEND_HOST).authenticate("invalid_token")
            }
        }
    }
}